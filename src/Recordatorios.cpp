#include <iostream>
#include <list>
#include <vector>


using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);//9
    void incrementar_dia();//10
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    #endif
  private:
    int mes_;
    int dia_;
    bool igual_dia;
    bool igual_mes;

    //Completar miembros internos
};

Fecha::Fecha(int mes, int dia) :  mes_(mes) , dia_(dia) {
};

int Fecha::mes() {
    return mes_;
}


int Fecha::dia(){
    return dia_;
}
//8

ostream& operator<<(ostream& os, Fecha f) {
    os << "" << f.dia() << "/" << f.mes() << "";
    return os;
}
//9

//EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this-> dia() == o.dia();
    bool igual_mes = this-> mes() == o.mes();
     //Completar iguadad (ej 9)

    return igual_dia == igual_mes;
}

//10
void Fecha::incrementar_dia(){
    if (dias_en_mes(mes())>=dia()){
        dia_ = dia_ + 1;
        if( dia_ > dias_en_mes(mes_)){
            mes_=mes_+1;
            dia_ = 1;
        }
    }
}


// Ejercicio 11, 12

//11
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator<(Horario h);
    bool operator==(Horario h);

private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min):hora_(hora),min_(min){
}

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << "" << h.hora() << ":" << h.min() << "";
    return os;
}



//12
bool Horario::operator<(Horario h) {
   if (hora() == h.hora() && min()==h.min()){
       return true;
   } else if (h.hora() > hora()){
        return true;
    }
   else if (hora() == h.hora() && min()<h.min()){
       return true;
   }
   else{
       return false;
   }
}



// Ejercicio 13

class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario horario , string mensaje);
    Fecha fecha();
    Horario horario();
    string mensaje();
    bool operator<(Recordatorio r);

private:
    Fecha fecha_;
    Horario horario_;
    string mensaje_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario , string mensaje): fecha_(fecha),horario_(horario), mensaje_(mensaje){
}

Fecha Recordatorio::fecha(){
    return fecha_;
}

Horario Recordatorio::horario(){
    return horario_;
}

string Recordatorio::mensaje(){
    return mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << "" << r.mensaje() << " @ " << r.fecha() << " " << r.horario()<< "";
    return os;
}


// Ejercicio 14

//Antes que nada arriba incluir el   #include <list>
//#include <vector> , para que la lista y el vector anden bien

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    //siguiendo los ejemplos de ejercicios anteriores agrego recordatorios_de_hoy y hoy_
    list<Recordatorio> recordatorios_de_hoy_;
    Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_incial ): hoy_(fecha_incial){
}

Fecha Agenda::hoy() {
    return hoy_;
}

//para agregar recordatorios pense en usar recordatoios_de_hoy y colorca ahi los de rec
void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_de_hoy_.push_back(rec);
}

//Use la funcion del punto 10 incrementar_dia en hoy_
void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}



list<Recordatorio> Agenda::recordatorios_de_hoy() {
    //Declaro Variables que me puedan servir
    list <Recordatorio> l1;
    vector<Recordatorio> v1;
    vector<Recordatorio> v2;


    /*Hago que la  elementos de lista recordatorios_de_hoy(que es igual a recordatorios_de_hoy_)
     se vuelquen en el vector v1 ya que es mucho mas facil moverme en vectores que en lista para mi*/

    while (recordatorios_de_hoy_.size() > 0) {
        v1.push_back(recordatorios_de_hoy_.front());
        recordatorios_de_hoy_.pop_front();
    }

    //Ahora los recordatorios que tenga la fecha==hoy() deberan volcarse en v2
    for (int i = 0; i < v1.size(); i++) {
        if (v1[i].fecha() == hoy()) {
            v2.push_back(v1[i]);
        }
    }

    //Aca  ordeno por horarios(hora y minuto) los registros ,de menor a mayor de v2
    for (int j = 0; j < v2.size(); j++) {
        for (int h = j+1; j < v2.size(); h++){
            if(v2[j].horario().hora()>v2[h].horario().hora() ||
            (v2[j].horario().hora()==v2[h].horario().hora() && v2[j].horario().min()>v2[h].horario().min()) ){
                swap(v2[j],v2[h]);
            }
        }
    }

    //ahora como todo esta bien ordenado en v2 ,paso sus registros a las lista l1 y listo
    for (int y = 0; y < v2.size(); y++){
        l1.push_back(v2[y]);
    }
    return l1;
}


//para imprimir
ostream& operator<<(ostream& os, Agenda a){

    os << a.hoy() << endl;
    os << "====" << endl;

    while(a.recordatorios_de_hoy().size() > 0){
        os << a.recordatorios_de_hoy().front() << endl;
        a.recordatorios_de_hoy().pop_front();
    }
    return os;
}


/*en Agenda{Ordenado ,desordenado ,Cambio_dia} explota todo(con esto no digo que marque errores ,
 * sino que aparece textos como Process finished with exit code -1073741819 (0xC0000005))


 * yo creo que es un problema que ocurre cuando quiero sacar algo de la lista recordatorios_de_hoy_
 * y colocarselo a un vector o lista (que no sera en este ejemplo pero probe usando solo listas)

1)  while (recordatorios_de_hoy_.size() > 0) {
2)        v1.push_back(recordatorios_de_hoy_.front());
3)      recordatorios_de_hoy_.pop_front();
4)  }

  * si en el programa yo comento la 2)  el programa marca error pero no explota al menos
  * demostrando que ahi ocurre algo extraño
*/